package com.skapps.myapplication;

import android.app.IntentService;
import android.content.Intent;
import android.nfc.Tag;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.List;

public class GeofenceTransitionService extends IntentService {

    //private static final String TAG = ;

    public GeofenceTransitionService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        GeofencingEvent geofencingEvent=GeofencingEvent.fromIntent(intent);

        if(geofencingEvent.hasError())
        {
            String error=String.valueOf(geofencingEvent.getErrorCode());
            Toast.makeText(getApplicationContext(),"Error code = "+error,Toast.LENGTH_SHORT).show();
            return;
        }

        int geofenceTransition=geofencingEvent.getGeofenceTransition();

        if(geofenceTransition== Geofence.GEOFENCE_TRANSITION_ENTER | geofenceTransition ==Geofence.GEOFENCE_TRANSITION_EXIT)
        {
            List<Geofence> triggeringgeofences=geofencingEvent.getTriggeringGeofences();

            String geoTransDetails=getGeofenceTransitionDetails(geofenceTransition,triggeringgeofences);



        }
    }

    private String getGeofenceTransitionDetails(int geofenceTransition, List<Geofence> triggeringgeofences) {

        ArrayList<String> triggerfencelist=new ArrayList<>();

        for(Geofence geofence : triggeringgeofences)
        {
            triggerfencelist.add(geofence.getRequestId());
        }

        String status=null;
        if(geofenceTransition==Geofence.GEOFENCE_TRANSITION_ENTER)
        {
            status="ENTERING";

        }
        else if(geofenceTransition==Geofence.GEOFENCE_TRANSITION_EXIT)
        {
            status="EXITING";
        }
        return status + TextUtils.join(", ",triggerfencelist);
    }
}

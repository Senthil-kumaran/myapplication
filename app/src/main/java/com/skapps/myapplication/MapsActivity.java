package com.skapps.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, LocationListener {

    private GoogleMap mMap;
    EditText e1;
    GoogleApiClient client;
    public LocationRequest request;
    Marker mm;
    Button s1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maplayout);
        e1 = (EditText) findViewById(R.id.editText);
        s1 = (Button) findViewById(R.id.bu);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        s1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MapsActivity.this, "GeoFence is clicked", Toast.LENGTH_SHORT).show();
                Intent q = new Intent(MapsActivity.this, MyLocationActivity.class);
                startActivity(q);
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
       /* gotoLocation(13.070919,80.159847,15);
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/

        if (mMap != null) {
            mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {

                }

                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    Geocoder gc = new Geocoder(MapsActivity.this);
                    List<Address> list = null;

                    try {
                        LatLng l1 = marker.getPosition();
                        list = gc.getFromLocation(l1.latitude, l1.longitude, 1);
                        Address address = list.get(0);
                        marker.setTitle(address.getLocality());
                        marker.showInfoWindow();
                    } catch (IOException e) {
                        e.getStackTrace();
                    }
                }
            });
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View row = getLayoutInflater().inflate(R.layout.custom_address, null);
                    TextView t1_locality = (TextView) row.findViewById(R.id.locality);
                    TextView t2_latitude = (TextView) row.findViewById(R.id.latTxt);
                    TextView t3_longitude = (TextView) row.findViewById(R.id.lngTxt);
                    TextView t4_snippet = (TextView) row.findViewById(R.id.snippet);

                    LatLng ll = marker.getPosition();
                    t1_locality.setText(marker.getTitle());
                    t2_latitude.setText(String.valueOf(ll.latitude));
                    t3_longitude.setText(String.valueOf(ll.longitude));
                    t4_snippet.setText(marker.getSnippet());
                    return row;
                }
            });
        }

        client = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this).build();

        client.connect();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        request = new LocationRequest().create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(1000);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(client, request, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location == null)
        {
            Toast.makeText(getApplicationContext(),"Location could not be found",Toast.LENGTH_SHORT).show();
        }
        else
        {
            LatLng latLng=new LatLng(location.getLatitude(),location.getLongitude());
            CameraUpdate update=CameraUpdateFactory.newLatLngZoom(latLng,15);
            mMap.animateCamera(update);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.none:
                mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                break;
            case R.id.normal:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.satellite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.hybrid:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case R.id.terrain:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void gotoLocation(double latitude, double longitude, int zoom)
    {
        LatLng lating=new LatLng(latitude,longitude);
        CameraUpdate update=CameraUpdateFactory.newLatLngZoom(lating,15);
        mMap.moveCamera(update);
    }

    public void findOnMap(View v)
    {
        Geocoder geocoder=new Geocoder(this);
        try
        {
            //List<Address> mylist = new geocoder.getFromLocation(e1.getText().toString(),1);
        List<Address> mylist = geocoder.getFromLocationName(e1.getText().toString(),1);
        Address address=mylist.get(0);
        String locality=address.getLocality();
            Toast.makeText(getApplicationContext(),locality,Toast.LENGTH_SHORT).show();
        double lat=address.getLatitude();
        double lon=address.getLongitude();
        gotoLocation(lat,lon,15);

        if(mm!=null)
        {
            mm.remove();
        }
        MarkerOptions options=new MarkerOptions();
        options.title(locality);
        options.draggable(true);
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        options.snippet("I am here");
        options.position(new LatLng(lat,lon));

        mm=mMap.addMarker(options);
        mMap.addMarker(options);

        }catch(IOException e)
        {
            e.printStackTrace();
        }

    }

}

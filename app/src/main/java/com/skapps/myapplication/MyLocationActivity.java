package com.skapps.myapplication;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MyLocationActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener,
        ResultCallback<Status> {

    private GoogleMap mMap;
    GoogleApiClient client;
    LocationRequest request;
    LatLng latLngStart;
    Marker currentMarker;
    Marker mm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_location2);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

       // e1 = (EditText) findViewById(R.id.editText);
        //  Toolbar toolbar=(Toolbar) findViewById(R.id.myToolbar);
        // toolbar.setTitle("Current Location");
        // setSupportActionBar(toolbar);

      /*  if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }*/
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
        // Add a marker in Sydney and move the camera
      /*  LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/

        client = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        client.connect();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        request = new LocationRequest().create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(1000);


      //  if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
         //   return;
      //  }
      // LocationServices.FusedLocationApi.requestLocationUpdates(client, request, (com.google.android.gms.location.LocationListener) this);
    }

    @Override
    public void onLocationChanged(Location location) {

        mMap.clear();
        MarkerOptions mp= new MarkerOptions();
        mp.position(new LatLng(location.getLatitude(),location.getLongitude()));
        mp.title("My position");
        mMap.addMarker(mp);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),location.getLongitude()),15));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_navigation,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      //  if (item.getItemId() == android.R.id.home) ;
        //finish();
        if (item.getItemId() == R.id.start_geo) ;
        {
            startGeofence();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResult(@NonNull Status status) {
        drawGeofence();

    }

    Circle geoFenceLimits;

    private void drawGeofence() {
        if (geoFenceLimits != null) {
            geoFenceLimits.remove();
        }
        CircleOptions circleOptions = new CircleOptions()
                .center(geofenceMarker.getPosition()).strokeColor(Color.argb(50, 70, 70, 70))
                .fillColor(Color.argb(100, 150, 150, 150)).radius(400f);

        geoFenceLimits = mMap.addCircle(circleOptions);
    }

    GeofencingRequest geoRequest;

    private void startGeofence() {
        if (geofenceMarker != null) {
            Geofence geofence = createGeofence(geofenceMarker.getPosition(), 400f);
            geoRequest = createGeoRequest(geofence);
            addGeoFence(geofence);
        }
    }

    private void addGeoFence(Geofence geofence) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.GeofencingApi.addGeofences(client, geoRequest, createGeofencingPendingIntent())
                .setResultCallback(this);
    }

    PendingIntent geoFencePendingIntent;
    private PendingIntent createGeofencingPendingIntent() {
        if(geoFencePendingIntent!=null)
        {
            return geoFencePendingIntent;
        }

        Intent i=new Intent(this,GeofenceTransitionService.class);

        return PendingIntent.getService(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private GeofencingRequest createGeoRequest(Geofence geofence) {
        return  new GeofencingRequest.Builder().setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                .addGeofence(geofence).build();
    }

    private Geofence createGeofence(LatLng position, float v) {
          return new Geofence.Builder().setRequestId("My Geofence")
          .setCircularRegion(position.latitude,position.longitude,v)
                  .setExpirationDuration(60 * 60 * 1000)
                  .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT).build();}

    @Override
    public void onMapClick(LatLng latLng) {
        markerForGeofence(latLng);
    }

    Marker geofenceMarker;
    private void markerForGeofence(LatLng latLng)
    {
        MarkerOptions optionsMarker=new MarkerOptions().position(latLng)
                .title("Geofence Marker");

        if(mMap!=null)
        {
            if(geofenceMarker!=null)
            {
                geofenceMarker.remove();
            }

            geofenceMarker=mMap.addMarker(optionsMarker);

        }
    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

   /* public void findOnMap(View v)
    {
        Geocoder geocoder=new Geocoder(this);
        try
        {
            //List<Address> mylist = new geocoder.getFromLocation(e1.getText().toString(),1);
            List<Address> mylist = geocoder.getFromLocationName(e1.getText().toString(),1);
            Address address=mylist.get(0);
            String locality=address.getLocality();
            Toast.makeText(getApplicationContext(),locality,Toast.LENGTH_SHORT).show();
            double lat=address.getLatitude();
            double lon=address.getLongitude();
            gotoLocation(lat,lon,15);

            if(mm!=null)
            {
                mm.remove();
            }
            MarkerOptions options=new MarkerOptions();
            options.title(locality);
            options.draggable(true);
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            options.snippet("I am here");
            options.position(new LatLng(lat,lon));

            mm=mMap.addMarker(options);
            mMap.addMarker(options);

        }catch(IOException e)
        {
            e.printStackTrace();
        }

    }

    public void gotoLocation(double latitude, double longitude, int zoom)
    {
        LatLng lating=new LatLng(latitude,longitude);
        CameraUpdate update=CameraUpdateFactory.newLatLngZoom(lating,15);
        mMap.moveCamera(update);
    }*/

}
